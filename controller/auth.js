const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { pool } = require("../dbconfig/dbConnection");


const login = async (req, res) => {
  // console.log(req.body);
  const username = req.body.username;
  const password = req.body.password;
  try {
    const result = await pool.query("SELECT * FROM users WHERE name = $1", [
      username
    ]);
    const user = result.rows[0];
    // console.log(user);
    // console.log(username);
    // console.log(password);

    if (result.rows.length === 0) {
      console.log("NO DATA!!");
      return res.status(401).json({ message: "Invalid credentials" });
    }

    // const user = result.rows[0];
    // console.log(user);
    const isPasswordValid = await bcrypt.compare(password, user.password);
    console.log(isPasswordValid);

    //     if (!isPasswordValid) {
    //       return res.status(401).json({ message: "Invalid credentials" });
    //     }

    const token = jwt.sign({ userId: user.id }, "your_secret_key");
    // res.json({ token });
    // console.log(token);
    res.send("verified");
    // res.redirect('/main');
  } catch (error) {
    console.error("Error during login:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const register = async (req, res) => {
  const { username, password } = req.body;

  try {
    // Check if the username already exists
    const userExists = await pool.query(
      "SELECT * FROM users WHERE username = $1",
      [username]
    );

    if (userExists.rows.length > 0) {
      return res.status(400).json({ message: "Username already exists" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    // Insert the new user into the database
    const newUser = await pool.query(
      "INSERT INTO users (username, password) VALUES ($1, $2) RETURNING id",
      [username, hashedPassword]
    );

    const userId = newUser.rows[0].id;
    const token = jwt.sign({ userId }, "your_secret_key");
    res.status(201).json({ token });
  } catch (error) {
    console.error("Error during registration:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = {
  login,
  register,
};
