// Create a new pool for database connections
const { Pool } = require("pg");
// const pool = new Pool({
//   user: "postgres",
//   password: "Abhishek4933@",
//   database: "test",
//   port: 5432,
//   host: "localhost",
// });

const pool = new Pool({
  connectionString: "postgres://hari_bank_shibir_user:3ZYbz5WIjsgWApI7rj46e8nTBSFmvDd7@dpg-ck5hrbui9prc73c53mmg-a.singapore-postgres.render.com/hari_bank_shibir",
  ssl: true,
  keepAlive: true,
});

const connectDB = async function () {
  try {
    await pool.connect();
    console.log("Successfully connected to the Database.");
  } catch (error) {
    console.log("Error in connecting to the database .", error);
  }
};

module.exports = { connectDB,pool };
