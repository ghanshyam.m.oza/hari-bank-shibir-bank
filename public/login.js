document.getElementById("login").onclick = async function () {
  let headersList = {
    Accept: "*/*",
    'Content-Type': "application/json",
  };
  let username = document.getElementById("uname").value;
  let password = document.getElementById("password").value;

  let bodyContent = JSON.stringify({
    username: username,
    password: password,
  });


  // console.log(bodyContent);

  let response = await fetch("http://localhost:3000/login", {
    method: "POST",
    body: bodyContent,
    headers: headersList,
  });

  let data = await response.text();
  console.log(data)
  if (data == "verified") {
    location.href = "http://localhost:3000/main";
  }
};
