function nameValidation(name) {
    const nameRegex = /^[a-zA-Z]+[{ }[a-zA-Z]+]*$/;

    // if (name == "" || (!(nameRegex.test(name))) || name == 'undefined') {
    //     return false;
    // }
    if(name==""){
        return false;
    }
    else {
        return true;
    }
}

function phoneValidation(phone) {
    const phoneRegex = /^[0-9]{10}$/;
    console.log(phone);

    if (phone == "" || (!(phoneRegex.test(phone))) || phone == undefined) {
        return false;
    }
    else {
        return true;
    }
}

function amountValidation(amount) {

    if (amount == 0) {
        return false;
    }
    else {
        return true;
    }
}

function passwordValidation(){
    const pwds = document.getElementsByClassName('form-control');
    const password = pwds[0].value;
    const confirmPassword = pwds[1].value;
    if(password != confirmPassword){
        document.getElementsByClassName('validationMessage')[0].classList.remove('disp');
        return false;
    }
    else{
        return true;
    }
}
