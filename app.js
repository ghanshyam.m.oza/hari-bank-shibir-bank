const express = require('express')
const http = require('http');
const sqlite3 = require('sqlite3').verbose()
const bodyParser = require('body-parser')
const session = require('express-session')
const cors = require('cors');
const path = require("path");
const fs = require("fs")
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
//const tableify = require('html-tableify');
const tableify = require("tableify")
const app = express()
const { pool } = require('./dbconfig/dbConnection');
const authRoutes = require('./routes/auth');
//SMK Id	Kutumb Id	Zone	Sub Zone	Gender	SMK Type	Head	Name	Middle	Surname	Full Name Eng	Present City/Village	Native City/Village	Mobile 1	Mobile 2	Email	Pan No.	Aadhar No.	DOB	Bhakt Status Name	AddressDescription	Education	Column1	FullNameGuj	Present City/Village Guj	Native Guj	Zone Name Guj	Sub Zone Name Guj	SMK Type Guj
// Use body-parser to parse form data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
app.use(bodyParser.json());
app.use(express.static(__dirname));
app.use(authRoutes.routers);
app.set('view engine', 'ejs');
// Use express-session to store user information
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}))

// app.post("/login", async (req, res) => {
//   const username = req.body.username;
//   const password = req.body.password;
//   try {
//     const result = await pool.query("SELECT * FROM users WHERE name = $1", [
//       username,
//     ]);

//     if (result.rows.length === 0) {
//       console.log("NO DATA!!");
//       return res.status(401).json({ message: "Invalid credentials" });
//     }

//     const user = result.rows[0];
//     console.log(user);
//     const isPasswordValid = await bcrypt.compare(password, user.password);
//     console.log(isPasswordValid);

// //     if (!isPasswordValid) {
// //       return res.status(401).json({ message: "Invalid credentials" });
// //     }

//     const token = jwt.sign({ userId: user.id }, "your_secret_key");
//     // res.json({ token });
//     console.log(token);
//     res.render('createUser.ejs',{success:undefined});
//     // res.redirect('/main');
//   } catch (error) {
//     console.error("Error during login:", error);
//     res.status(500).json({ message: "Internal Server Error" });
//   }
// });

app.get('/', (req, res) => {
  //res.sendFile(__dirname + '/login.html')
  res.render("login", { styleUrl: "./style.css", title: "Login", heading: "Login", usernameLabel: "username", passwordLabel: "password", resetButtonLabel: "Reset", loginButtonLabel: "Login" })
})

app.get('/signup', (req, res) => {
  res.sendFile(__dirname + '/signup.html')
})

app.post('/signup', (req, res) => {
  const username = req.body.username
  const password = req.body.password

  // Connect to the SQLite database
  let db = new sqlite3.Database('./users.db', (err) => {
    if (err) {
      console.error(err.message)
    }
    console.log('Connected to the users database.')
  })

  // Insert new user into the database
  let sql = `INSERT INTO users(username, password) VALUES(?,?)`
  db.run(sql, [username, password], function (err) {
    if (err) {
      return console.log(err.message);
    }
    console.log(`A row has been inserted with rowid ${this.lastID}`);
  });
  db.close();
  res.redirect('/login');
});

// app.post('/login', (req, res) => {
//   const username = req.body.username
//   const password = req.body.password
//   // Connect to the SQLite database
//   let db = new sqlite3.Database('./users.db', (err) => {
//     if (err) {
//       console.error(err.message)
//     }
//     console.log('Connected to the users database.')
//   })

//   // Check the login credentials
//   let sql = `SELECT * FROM users WHERE username = ? AND password = ?`
//   db.get(sql, [username, password], (err, row) => {
//     if (err) {
//       console.error(err.message)
//     }
//     if (row) {
//       // Start a session
//       req.session.loggedin = true
//       req.session.username = username
//       res.send("verified")
//     } else {
//       res.send('Incorrect username and/or password!')
//     }
//     db.close()
//   })
// })

app.get('/logout', (req, res) => {
  req.session.destroy(err => {
    if (err) {
      return console.log(err);
    }
    res.redirect('/');
  });
});

app.get("/main", (req, res) => {
  const db = new sqlite3.Database('./transactions.db', sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to the smk database.');
  });
  const query = `
    SELECT 
    "SMKId",
    "FullNameGuj",
    "PresentCity",
    "NativeCity",
    "Mobile1",
    "Email",
    "Upad",
    "Dhotiya",
    "Abhishek",
    "Grahan",
    "Rasoi",
    "Thal",
    "Bhet",
    "Other",
    "Jama_Rakam",
    "Kul_Balance",
    "User",
    "Sr",
    "Date",
    "Kul_Upad"
    FROM "transaction";`;

  db.all(query, (err, rows) => {
    if (err) {
      console.error(err.message);
    }
    else {
      if (req.session.loggedin) {
        res.render("Main", { username: req.session.username, userserialno: "00001", data: rows[2] });
        console.log(rows);
      } else {
        res.redirect('/')
      }
    }
    //res.send(rows[0])
    //const template = fs.readFileSync('./views/test.ejs', 'utf-8');
    //res.render("test", {data:rows[2]});
    //  res.send(tableify(rows))
    //   rows.forEach((row) => {
    //     console.log('SMKId', row.SMKId);
    //     console.log('FullNameGuj', row.FullNameGuj);
    //     console.log('PresentCity', row.PresentCity);
    //     console.log('NativeCity', row.NativeCity);
    //     console.log('Mobile1', row.Mobile1);
    //     console.log('Email', row.Email);
    //     console.log('Upad', row.Upad);
    //     console.log('Dhotiya', row.Dhotiya);
    //     console.log('Abhishek', row.Abhishek);
    //     console.log('Grahan', row.Grahan);
    //     console.log('Rasoi', row.Rasoi);
    //     console.log('Thal', row.Thal);
    //     console.log('Bhet', row.Bhet);
    //     console.log('Other', row.Other);
    //     console.log('Jama_Rakam', row.Jama_Rakam);
    //     console.log('Kul_Balance', row.Kul_Balance);
    //     console.log('User', row.User);
    //     console.log('Sr', row.Sr);
    //     console.log('Date', row.Date);
    //     console.log('Kul_Upad', row.Kul_Upad);
    //     console.log('\n');
    //   });
  });

  db.close();

})

app.get("/search", (req, res) => {
  if (req.session.loggedin) {
    const db = new sqlite3.Database('./smk.db', sqlite3.OPEN_READONLY, (err) => {
      if (err) {
        console.error(err.message);
      }
      console.log('Connected to the smk database.');
    });

    //   db.run("PRAGMA journal_mode = WAL;", (err) => {
    //     if (err) {
    //       console.error(err.message);
    //     }
    //     console.log('WAL enabled.');
    //   });
    db.get("Select SMKId,FullNameGuj,PresentCity,NativeCIty,Mobile1,Email FROM smk WHERE SMKId=" + req.query.smk + ";", (err, rows) => {
      console.log("Select SMKId,FullNameGuj,PresentCity,NativeCIty,Mobile1,Email FROM smk WHERE SMKId=" + req.query.smk + ";")
      if (err) {
        res.send(err)
      }
      else {
        res.send(JSON.stringify(rows));
      }

    });
    db.close();
  } else {
    res.redirect('/')
  }
  // if (req.session.loggedin) {

  //  } else {
  //    res.redirect('/')
  //  }

})

app.post("/transact", (req, res) => {
  //if (req.session.loggedin) {
  const db = new sqlite3.Database('./transactions.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to the smk database.');
  });

  db.run("PRAGMA journal_mode = WAL;", (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('WAL enabled.');
  });

  db.serialize(function () {
    db.run("BEGIN;")
    db.run(`INSERT INTO "transaction" ("SMKId", "FullNameGuj", "PresentCity", "NativeCity", "Mobile1", "Email", "Upad", "Dhotiya", "Abhishek", "Grahan", "Rasoi", "Thal", "Bhet", "Other", "Jama_Rakam", "Kul_Balance", "User", "Sr", "Date","Kul_Upad")
                VALUES (${req.body.SMKId}, "${req.body.FullNameGuj}", "${req.body.PresentCity}", "${req.body.NativeCity}", ${req.body.Mobile1}, "${req.body.Email}", ${req.body.Upad}, ${req.body.Dhotiya}, ${req.body.Abhishek}, ${req.body.Grahan}, ${req.body.Rasoi}, ${req.body.Thal}, ${req.body.Bhet}, ${req.body.Other}, ${req.body.Jama_Rakam}, ${req.body.Kul_Balance}, "${req.body.User}", ${req.body.Sr}, "${req.body.Date}",${req.body.Kul_Upad});`, function (err, row) {
      if (err) {
        console.log(err);
        db.run("ROLLBACK;");
        res.end("Transaction cancelled");
      }
      else {
        db.run("COMMIT;")
        res.send("Transaction successful");
      }
    })
  })
  //db.close()
  // } else {
  // res.redirect('/')
  // }
})

app.get("/gethistory", (req, res) => {
  //if (req.session.loggedin) {
  const db = new sqlite3.Database('./transactions.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to the smk database.');
  });
  db.all("SELECT * FROM " + "\"transaction\"" + " where User=" + "\"" + req.query.username + "\"" + " ORDER BY ID DESC LIMIT 100 ", (err, rows) => {
    if (err || rows.length == 0) {
      console.error(err);
      res.sendStatus(404);
    }
    else {
      //rows = JSON.stringify(rows)
      //console.log(JSON.stringify(rows[0]))
      //var demoTable = tableify(rows);
      //console.log(demoTable)
      const demoTable = tableify(rows.map(row => {
        for (const key in row) {
          if (row[key] === 0) {
            row[key] = '0';
          }
        }
        return row;
      }));
      res.send(demoTable)
    }
  })
  // } else {
  //     res.redirect('/')
  // }
  //db.all("")  
  //db.close()

})

app.get("/getlasthistory", (req, res) => {
  //if (req.session.loggedin) {
  const db = new sqlite3.Database('./transactions.db', sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to the smk database.');
  });
  db.all("SELECT * FROM " + "\"transaction\"" + " where User=" + "\"" + req.query.username + "\"" + " AND SMKId=" + req.query.smk + " ORDER BY ID DESC LIMIT 1 ", (err, rows) => {
    console.log("SELECT * FROM " + "\"transaction\"" + " where User=" + "\"" + req.query.username + "\"" + " AND SMKId=" + req.query.smk + " ORDER BY ID DESC LIMIT 1 ")
    if (err || rows.length == 0) {
      console.error(err);
      res.sendStatus(404);
    }
    else {
      //var demoTable = tableify(rows);
      res.send(JSON.stringify(rows))
    }
  })
  // } else {
  //     res.redirect('/')
  // }
  //db.all("")  
  //db.close()

})

app.get("/userhistory", (req, res) => {
  //if (req.session.loggedin) {
  const db = new sqlite3.Database('./transactions.db', sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to the smk database.');
  });
  db.all("SELECT COUNT(*) as Countrows, SUM(Upad) As Upad,SUM(Dhotiya) As Dhotiya,SUM(Abhishek) As Abhishek,SUM(Grahan) As Grahan,SUM(Rasoi) As Rasoi,SUM(Thal) As Thal,SUM(Bhet) As Bhet, SUM(Other) As Other,SUM(Kul_Upad) As Kul_Upad,SUM(Jama_Rakam) As Jama_Rakam FROM " + "\"transaction\"" + " where User=" + "\"" + req.query.username + "\"", (err, rows) => {
    if (err || rows.length == 0) {
      console.error(err);
      res.sendStatus(404);
    }
    else {
      //var demoTable = tableify(rows);
      res.send(JSON.stringify(rows))
    }
  })
  // } else {
  //     res.redirect('/')
  // }
  //db.all("")  
  //db.close()

})

app.get("/test", (req, res) => {
  const db = new sqlite3.Database('./transactions.db', sqlite3.OPEN_READONLY, (err) => {
    if (err) {
      console.error(err.message);
    }
    console.log('Connected to the smk database.');
  });
  const query = `
SELECT 
  "SMKId",
  "FullNameGuj",
  "PresentCity",
  "NativeCity",
  "Mobile1",
  "Email",
  "Upad",
  "Dhotiya",
  "Abhishek",
  "Grahan",
  "Rasoi",
  "Thal",
  "Bhet",
  "Other",
  "Jama_Rakam",
  "Kul_Balance",
  "User",
  "Sr",
  "Date",
  "Kul_Upad"
FROM "transaction";`;

  db.all(query, (err, rows) => {
    if (err) {
      console.error(err.message);
    }
    //res.send(rows[0])
    //const template = fs.readFileSync('./views/test.ejs', 'utf-8');
    res.render("test", { data: rows[2] });
    //  res.send(tableify(rows))
    //   rows.forEach((row) => {
    //     console.log('SMKId', row.SMKId);
    //     console.log('FullNameGuj', row.FullNameGuj);
    //     console.log('PresentCity', row.PresentCity);
    //     console.log('NativeCity', row.NativeCity);
    //     console.log('Mobile1', row.Mobile1);
    //     console.log('Email', row.Email);
    //     console.log('Upad', row.Upad);
    //     console.log('Dhotiya', row.Dhotiya);
    //     console.log('Abhishek', row.Abhishek);
    //     console.log('Grahan', row.Grahan);
    //     console.log('Rasoi', row.Rasoi);
    //     console.log('Thal', row.Thal);
    //     console.log('Bhet', row.Bhet);
    //     console.log('Other', row.Other);
    //     console.log('Jama_Rakam', row.Jama_Rakam);
    //     console.log('Kul_Balance', row.Kul_Balance);
    //     console.log('User', row.User);
    //     console.log('Sr', row.Sr);
    //     console.log('Date', row.Date);
    //     console.log('Kul_Upad', row.Kul_Upad);
    //     console.log('\n');
    //   });
  });

  db.close();
})

app.listen(3000, () => {
  console.log('Server started on http://localhost:3000')
})