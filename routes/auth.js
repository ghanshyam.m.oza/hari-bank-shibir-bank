const express = require("express");

const { login, register } = require( "../controller/auth");

const routers = require("express").Router();


routers.post("/login", login);
routers.post("/register", register);


module.exports={
    routers
}