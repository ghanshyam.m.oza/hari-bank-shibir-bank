const express = require("express");
const routers = require("express").Router();

routers.get("/main", main)
routers.post("/transact", transact)
routers.get("/search", search)
routers.get("/gethistory", gethistory)
routers.get("/getlasthistory", getlasthistory)
routers.get("/userhistory", userhistory)
